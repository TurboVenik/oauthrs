package com.example.demo.controller;

import com.example.demo.controller.model.CheckContent;
import com.example.demo.controller.model.CheckRequest;
import com.example.demo.controller.model.CheckResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/public")
public class PublicController {

    @RequestMapping(
            value = "/external/check",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> check(@Valid @RequestBody CheckRequest request) {
        CheckResponse checkResponse =
                new CheckResponse(0, new CheckContent("approved", "Ok."));

        HttpStatus httpStatus
                = checkResponse.getRespcode() == 0 ? HttpStatus.OK : HttpStatus.NOT_FOUND;

        return new ResponseEntity<>(checkResponse, httpStatus);
    }
}
