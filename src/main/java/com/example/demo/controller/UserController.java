package com.example.demo.controller;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/api/user")
@CrossOrigin(origins = "*")
public class UserController {

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String hello() {
        Object details = SecurityContextHolder.getContext().getAuthentication().getDetails();
        OAuth2AuthenticationDetails oauthDetails
                = (OAuth2AuthenticationDetails) SecurityContextHolder.getContext().getAuthentication().getDetails();
        Map<String, Object> decodedDetails = (Map<String, Object>)  oauthDetails.getDecodedDetails();
        if (decodedDetails != null) {
            System.out.println((String) decodedDetails.get("mail"));
            System.out.println( decodedDetails.get("m_id") == null ? null : (int)decodedDetails.get("m_id"));
        }

        return "hello " + SecurityContextHolder.getContext().getAuthentication().getName();
    }
}
