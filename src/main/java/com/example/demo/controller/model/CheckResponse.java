package com.example.demo.controller.model;

import com.example.demo.controller.QrController;

public class CheckResponse {
    /*
    Код возврата(0 - успешно, 1 - неуспешно)
    */
    private int respcode;
    /*
        Доп. информация
     */
    private CheckContent content;

    public CheckResponse(int respcode, CheckContent content) {
        this.respcode = respcode;
        this.content = content;
    }

    public int getRespcode() {
        return respcode;
    }

    public void setRespcode(int respcode) {
        this.respcode = respcode;
    }

    public CheckContent getContent() {
        return content;
    }

    public void setContent(CheckContent content) {
        this.content = content;
    }
}