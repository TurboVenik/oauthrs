package com.example.demo.controller.model;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class CheckRequest {
    @NotNull(message = "Не указана сумма платежа.")
    private BigDecimal amount;

    @NotNull(message = "Не указан идентификатор транзакции.")
    private Long auth_id;

    @NotNull(message = "Не указан идентификатор терминала.")
    private Long terminal_id;

    @NotNull(message = "Не указан идентификатор коммерсанта.")
    private Long merch_id;

    public CheckRequest(){}

    public CheckRequest(@NotNull(message = "Не указана сумма платежа.") BigDecimal amount,
                        @NotNull(message = "Не указан идентификатор транзакции.") Long auth_id,
                        @NotNull(message = "Не указан идентификатор терминала.") Long terminal_id,
                        @NotNull(message = "Не указан идентификатор коммерсанта.") Long merch_id) {
        this.amount = amount;
        this.auth_id = auth_id;
        this.terminal_id = terminal_id;
        this.merch_id = merch_id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Long getAuth_id() {
        return auth_id;
    }

    public void setAuth_id(Long auth_id) {
        this.auth_id = auth_id;
    }

    public Long getTerminal_id() {
        return terminal_id;
    }

    public void setTerminal_id(Long terminal_id) {
        this.terminal_id = terminal_id;
    }

    public Long getMerch_id() {
        return merch_id;
    }

    public void setMerch_id(Long merch_id) {
        this.merch_id = merch_id;
    }
}