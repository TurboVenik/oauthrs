package com.example.demo.controller.model;

public class CheckContent {
    /*
        Статус платежа
    */
    private String status;
    /*
        Дополнительная информация(пока ничего не передаем)
    */
    private String additional_data;

    public CheckContent(String status, String additional_data) {
        this.status = status;
        this.additional_data = additional_data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAdditional_data() {
        return additional_data;
    }

    public void setAdditional_data(String additional_data) {
        this.additional_data = additional_data;
    }
}
