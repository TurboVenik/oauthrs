package com.example.demo.security;

import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class MyClassFilter implements Filter {

    @Override
    public void destroy() {}

    @Override
    public void init(FilterConfig arg0) throws ServletException {}

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
//        if (((HttpServletRequest) request).getMethod().equals("OPTIONAL")) {
//            ((HttpServletResponse) response).addHeader("Access-Control-Allow-Origin", "*");
//            ((HttpServletResponse) response).addHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
//            ((HttpServletResponse) response).addHeader("Access-Control-Allow-Headers", "Authorization");
//        }
        chain.doFilter(request, response);
    }
}
